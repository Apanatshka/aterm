#![feature(test)]

extern crate aterm;
extern crate test;
extern crate fnv;

use test::{Bencher, black_box};

use aterm::rc::{ATermFactory as Factory};
use aterm::parse::ATermRead;

use std::collections::hash_map::RandomState;

const SMALL_ATERM: &'static str = r#"
    banana("hi"(son,nanomachines), [this, is, 1, small, "...", aterm])
"#;

const LIB_STRATEGO: &'static str = include_str!("libstratego-lib-posix-xsi.ctree");

#[bench]
fn bench_read_small(b: &mut Bencher) {
    b.iter(|| {
        let factory: Factory<(), RandomState> = Factory::new();
        let _ = black_box(factory.read_ascii(SMALL_ATERM));
    });
}

#[bench]
fn bench_read_stdlib(b: &mut Bencher) {
    b.iter(|| {
        let factory: Factory<(), RandomState> = Factory::new();
        let _ = black_box(factory.read_ascii(LIB_STRATEGO));
    });
}

#[bench]
fn bench_read_small_fnv(b: &mut Bencher) {
    b.iter(|| {
        let factory: Factory<(), fnv::FnvBuildHasher> = Factory::new();
        let _ = black_box(factory.read_ascii(SMALL_ATERM));
    });
}

#[bench]
fn bench_read_stdlib_fnv(b: &mut Bencher) {
    b.iter(|| {
        let factory: Factory<(), fnv::FnvBuildHasher> = Factory::new();
        let _ = black_box(factory.read_ascii(LIB_STRATEGO));
    });
}
