This repository holds an implementation of the Annotated Term (ATerm) format in Rust. 

# Annotated Terms

I suspect that originally ATerms were a big thing because of the convenience of a garbage collected C implementation of
arbitrary trees, with _maximal sharing_. Now it seems to be more of a legacy format. You can still find the [old ATerm
guide](http://homepages.cwi.nl/~daybuild/daily-books/technology/aterm-guide/aterm-guide.html) online but I'll briefly
recap the format.

## Format recap

This format encodes trees. The leaves of the trees can be:

- Integers (`i32`)
- Longs (`i64`, optional in the spec)
- Strings (`String`[^*])
- Reals (`f32`)
- Blobs (`Vec<u8>`, though when you need this, are ATerms really what you need?)
- Placeholders (`TermPlaceholder`, typed holes, for the legacy pattern interface)

The trees are built up with _applications_ of _constructors_. A constructor is just an alphanumeric name or a quoted
string. The application of the constructor has zero or more children. 

There are also specialised lists, mostly because they have their own syntax.

Tuples are applications of the empty constructor. 

[^*]: actually they're just quoted constructors with zero children

Oh right, and they're _annotated_: every term can have a list of annotations, which are also terms. Lists and
constructor applications are also counted as terms, so basically everything can have annotations. 

## Features

The Rust implementation has the follow features / todos:
 
- [X] Longs
- [X] Reading/writing ASCII (textual aterms)
- [X] Utility functions? (Some todos: #7)
- [X] Maximal sharing (Some todos: #3)
- [ ] Reading/writing TAF #4
- [ ] Reading/writing BAF #5
- [ ] Reading/writing SAF #6
