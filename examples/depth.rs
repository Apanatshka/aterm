extern crate aterm;

use aterm::TermPlaceholder;
use aterm::rc::{Term, ATerm as FactoryATerm, ATermFactory as Factory};
use aterm::parse::ATermRead;

use std::fs::File;
use std::io::Read;
use std::io::Result;
use std::process;
use std::collections::hash_map::RandomState;
use std::rc::Rc;

fn main() {
    if let Some(file) = std::env::args().nth(1) {
        if let Err(e) = exec(file) {
            use std::io::{Write, stderr};
            writeln!(&mut stderr(), "{}", e).unwrap();
            process::exit(1);
        }
    } else {
        println!(
            "Give one argument, a file with an ASCII encoded ATerm. The depth of the tree \
                  will be returned then. "
        )
    }
}

fn exec(file: String) -> Result<usize> {
    let mut file = File::open(file)?;
    let mut contents = String::new();
    let _ = file.read_to_string(&mut contents);
    let factory: Factory<(), RandomState> = Factory::new();
    if let Ok((aterm, _)) = factory.read_ascii(&contents) {
        let depth = depth(&aterm);
        println!("{}", depth);
    }
    Ok(0)
}

fn depth(aterm: &Rc<FactoryATerm<()>>) -> usize {
    match aterm.term {
        Term::Int(_) | Term::Long(_) | Term::Real(_) | Term::Blob(_) => 1,
        Term::Application(_, ref rec) |
        Term::List(ref rec) => rec.into_iter().map(depth).max().unwrap_or(0) + 1,
        Term::Placeholder(ref p) => {
            match *p {
                TermPlaceholder::Application(ref rec) => {
                    rec.into_iter().map(depth).max().unwrap_or(0) + 1
                }
                _ => 1,
            }
        }
    }
}
