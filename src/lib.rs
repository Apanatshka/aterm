#[macro_use]
pub mod utils;

mod bad_idea;
pub mod rc;
mod interface;
pub mod print;
pub mod parse;
pub mod string_share;

pub use interface::*;

extern crate regex;

#[macro_use]
extern crate lazy_static;

extern crate typed_arena;
