use std::mem;
use std::hash;

/// An f32 with broken Eq and Hash implementations.
/// To quote shepmaster (except this is an f32):
/// > Basically, if you want to treat a f64 as a set of bits that have no meaning, well, we can
/// > treat them as an equivalently sized bag of bits that know how to be hashed and
/// > bitwise-compared.
/// > Don't be surprised when one of the 16 million NaN values doesn't equal another one.
/// http://stackoverflow.com/q/39638363/859279
#[derive(Debug, Clone, Copy)]
pub struct BrokenF32(pub f32);

impl BrokenF32 {
    fn key(&self) -> u32 {
        unsafe { mem::transmute(self.0) }
    }
}

impl hash::Hash for BrokenF32 {
    fn hash<H>(&self, state: &mut H)
    where
        H: hash::Hasher,
    {
        self.key().hash(state)
    }
}

impl PartialEq for BrokenF32 {
    fn eq(&self, other: &Self) -> bool {
        self.key() == other.key()
    }
}

impl Eq for BrokenF32 {}
