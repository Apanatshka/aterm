use std::fmt;
use std::hash::Hash;
use std::rc::Rc;
use std::borrow::Borrow;

pub trait ATermWrite {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result;

    fn to_ascii_string(&self) -> Result<String, fmt::Error> {
        let mut s = String::new();
        self.to_ascii(&mut s)?;
        Ok(s)
    }
}

impl<T: ATermWrite> ATermWrite for [T] {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        let mut iter = self.iter().peekable();
        while let Some(t) = iter.next() {
            t.to_ascii(writer)?;
            if iter.peek().is_some() {
                write!(writer, ",")?;
            }
        }
        Ok(())
    }
}

impl<'a> ATermWrite for &'a str {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        write!(writer, "{}", ::utils::string_escape(self))
    }
}

impl<'a> ATermWrite for String {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        write!(writer, "{}", self)
    }
}

impl<T: ATermWrite> ATermWrite for Rc<T> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        Borrow::<T>::borrow(self).to_ascii(writer)
    }
}

pub trait ATermWriteBlob: Hash {
    fn size(&self) -> usize;
}

impl<B: ATermWriteBlob> ATermWrite for B {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        use std::hash::Hasher;
        use std::collections::hash_map::DefaultHasher;

        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        write!(writer, "{}#{}", self.size(), hasher.finish())
    }
}
